CREATE TABLE igreja (
	id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	igreja_nome VARCHAR(100) NOT NULL,
	igreja_cnpj VARCHAR(20),	
	igreja_telefone1 VARCHAR(15),
	igreja_telefone2 VARCHAR(15),
	igreja_site VARCHAR(50),
	igreja_email VARCHAR(50),
	igreja_data_fundacao VARCHAR(10)	
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO igreja (igreja_nome) values ('1ª IGREJA PRESBITERIANA DE SALGUEIRO');