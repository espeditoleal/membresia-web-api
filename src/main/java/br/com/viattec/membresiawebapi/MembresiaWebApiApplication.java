package br.com.viattec.membresiawebapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MembresiaWebApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MembresiaWebApiApplication.class, args);
	}

}
